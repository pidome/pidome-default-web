
module.exports = {
    pluginOptions: {
        webpackBundleAnalyzer: {
            openAnalyzer: false
        }
    },
    devServer: {
        disableHostCheck: true
    }
};
