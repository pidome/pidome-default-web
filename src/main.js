import Vue from 'vue';
import App from '@/App.vue';
import vuetify from './plugins/vuetify';
import router from '@/core/router';
import {store} from '@/store/index';
import 'typeface-roboto';
import 'material-design-icons/iconfont/material-icons.css';
import fontawesomeicon from '@/core/helpers/FontAwesome.vue';

Vue.config.productionTip = false;

/// Components
import LoadingSpinner from '@/core/components/LoadingSpinner.vue';
Vue.component('loading-spinner', LoadingSpinner);

/// font awesome
Vue.component('font-awesome-icon', fontawesomeicon); // Register component globally

/// The global error popup.
import ErrorPopup from '@/core/components/error/ErrorPopup.vue';
Vue.component('error-popup', ErrorPopup);

import '@/views/styles/custom.styl'

import { HttpService } from '@/core/services/HttpService';

import Toaster from '@/core/helpers/Toaster';

new Vue({
    vuetify,
    render: h => h(App),
    router,
    store,
    iconfont: 'mdi' || 'faSvg',
    mounted() {
        this.$root.$ErrorPopup = new Vue(ErrorPopup).$mount();
        HttpService.registerErrorPopup(this.$root.$ErrorPopup);
        Toaster.setToastInstance(this.$toast);
    }
}).$mount('#app');
