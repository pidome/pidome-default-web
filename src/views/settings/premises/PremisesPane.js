import SVG from '@svgdotjs/svg.js/src/svg.js';
import '@svgdotjs/svg.panzoom.js';
//import { HttpService } from '@/core/services/HttpService';

/**
 * Planner editor exposed methods.
 * @type type
 */
class PremisesPane {

    /**
     * The pane in witch the SVG is present.
     */
    svgPane;

    /**
     * If editor mode should be enabled or not.
     *
     * By default it's not enabled.
     * @type Boolean
     */
    motionMode = false;

    /**
     * The click handler.
     *
     * The method to call when a click on any element is detected.
     * @type method
     */
    clickHandler;

    /**
     * If the click event is valid.
     * When panning or zooming this should be turned false.
     */
    isValidClickEvent = false;

    /**
     * Method to be executed when the mouse goes down.
     * @returns {void}
     */
    onMouseDownHandler;

    /**
     * Start coordinates when panning starts.
     */
    panStartCoords = {'screenX': 0, 'screenY': 0};

    /**
     * End coordinates when panning ends.
     */
    panEndCoords = {'screenX': 0, 'screenY': 0};

    /**
     * The hover handler.
     *
     * The mthod to call when the mouse enters an element.
     * @type method.
     */
    hoverhandler;

    /**
     * Initializes the SVG Pane.
     * @param {#DivElement} div The name of the div.
     * @param {boolean} editMode if editor mode should be enabled.
     * @returns {void}
     */
    constructor(div, editMode) {
        this.svgPane = SVG(div).size('100%', '100%').panZoom();
        const me = this;
        this.onMouseDownHandler = function () {
            me.isValidClickEvent = true;
        };
        this.svgPane.on('panStart', function (ev) {
            //console.log(ev);
            me.panStartCoords = {'screenX': ev.detail.event.screenX, 'screenY': ev.detail.event.screenY};
        });
        this.svgPane.on('panEnd', function (ev) {
            //console.log(ev);
            me.panEndCoords = {'screenX': ev.detail.event.screenX, 'screenY': ev.detail.event.screenY};
        });
        if (editMode !== undefined) {
            this.editorMode = editMode;
        }
    }

    /**
     * Registers a click handler when any element in the svg is clicked.
     * @param {type} clickHandler The method to execute when clicked.
     * @returns {void}
     */
    registerClickHandler(clickHandler) {
        if (this.editorMode) {
            const me = this;
            this.clickHandler = clickHandler;
            SVG.on(window, 'mousedown', this.onMouseDownHandler);
            SVG.on(window, 'mouseup', function (el) {
                //console.log(me.panStartCoords);
                //console.log(me.panEndCoords);
                if (me.panStartCoords.screenX === me.panEndCoords.screenX &&
                        me.panStartCoords.screenY === me.panEndCoords.screenY) {
                    me.clickHandler(el);
                }
            });
        }
    }

    /**
     * Unregisters any click handler registered.
     *
     * This unregisters any method that has been set. It also removes any references to
     * te given method supplied by registerClickHandler.
     * @returns {void}
     */
    unregisterClickHandler() {
        if (this.clickHandler !== undefined) {
            SVG.of(window, 'mousedown', this.onMouseDownHandler);
            SVG.off(window, 'mouseup', this.clickHandler);
            this.clickHandler = undefined;
        }
    }

    /**
     * Registers an handler for element hovering.
     * @param {method} hoverHandler The method to run when hovering over.
     * @returns {void}
     */
    registerHoverhandler(hoverHandler) {
        if (this.editorMode) {
            this.hoverHandler = hoverHandler;
            const me = this;
            SVG.on(window, 'mouseover', function (el) {
                me.isValidClickEvent = false;
                me.hoverHandler(el);
            });
        }
    }

    /**
     * Removes the hover handler.This unregisters any method that has been set. It also removes any references to
     * te given method supplied by registerHoverhandler.
     * @returns {void}
     */
    unregisterHoverHandler() {
        if (this.clickHandler !== undefined) {
            SVG.off(window, 'click', this.clickHandler);
            this.clickHandler = undefined;
        }
    }

    fill(element, color) {
        SVG.adopt(element).fill(color);
    }

}

export default PremisesPane;
