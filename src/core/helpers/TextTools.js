/**
 * Helper to provide the correct icon for any hardware type.
 * @returns {HardwareIconHelper}
 */

export const TextTools = {
    camelCaseToClassFieldType,
    ucFirst
};

/**
 * Uppercase first character in word
 * @param word word to uppercase first character.
 * @returns {string} First character uppercased word.
 */
function ucFirst(word){
    return word[0].toUpperCase() + word.slice(1);
}

/**
 * Returns the icon for an hardware interface.
 * @param givenIcon The hardware interface
 * @returns {string[]}
 */
function camelCaseToClassFieldType(fieldName){
    let replaced = fieldName.toLowerCase().replace(/([-_][a-z])/g, (group) => group.toUpperCase().replace('_', ''));
    return replaced.charAt(0).toUpperCase() + replaced.slice(1);
}
