/**
 * Presentation platform
 * @returns {undefined}
 */
const PresentationPlatform = function () {

    /**
     * Collection of used platforms.
     * @type Array
     */
    PresentationPlatform.prototype.presentationItems = {
        /**
         * Generic desktop.
         */
        'desktop': {'type': 'DESKTOP', 'icon': 'computer'},
        /**
         * Generic pc aka desktop
         */
        'pc': {'type': 'DESKTOP', 'icon': 'computer'},
        /**
         * Mobile device.
         */
        'mobile': {'type': 'MOBILE', 'icon': 'smartphone'},
        /**
         * Generic smartphone, alternative for mobile.
         */
        'smartphone': {'type': 'MOBILE', 'icon': 'smartphone'},
        /**
         * A tablet.
         */
        'tablet': {'type': 'TABLET', 'icon': 'tablet'},
        /**
         * Like a magic mirror.
         */
        'mirror': {'type': 'MIRROR', 'icon': 'assignment_ind'},
        /**
         * Like an home automation controller.
         */
        'controller': {'type': 'CONTROLLER', 'icon': 'developer_board'},
        /**
         * A gaming console.
         */
        'gaming': {'type': 'GAMING', 'icon': 'videogame_asset'},
        /**
         * A watch.
         */
        'watch': {'type': 'WATCH', 'icon': 'watch'},
        /**
         * A television.
         */
        'television': {'type': 'TELEVISION', 'icon': 'tv'},
        /**
         * Generic monitor.
         *
         * This will be used as a default if no specific can be identified.
         */
        'monitor': {'type': 'MONITOR', 'icon': 'desktop_windows'},
        /**
         * A generic appliance (mostly household)
         */
        'appliance': {'type': 'APPLIANCE', 'icon': 'devices_other'},
        /**
         * A car interface.
         */
        'car': {'type': 'CAR', 'icon': 'directions_car'},
        /**
         * A car interface.
         */
        'unknown': {'type': 'UNKNOWN', 'icon': 'devices_other'}
    };

    /**
     * Returns the presentation item known in PiDome based on WhichBrowser vars.
     * @param {String} item
     * @returns {PresentationPlatform.prototypepresentationItems.monitor.type}
     */
    PresentationPlatform.prototype.translatedType = function (item) {
        for (let key in this.presentationItems) {
            if (item === key) {
                return this.presentationItems[key].type;
            }
        }
        return this.presentationItems['monitor'].type;
    };

    /**
     * Returns the icon of the type.
     * @param {String} item
     * @returns {PresentationPlatform.prototypepresentationItems.monitor.icon}
     */
    PresentationPlatform.prototype.icon = function (item) {
        for (let key in this.presentationItems) {
            if (item === key || this.presentationItems[key].type === item) {
                return this.presentationItems[key].icon;
            }
        }
        return this.presentationItems['unknown'].icon;
    };

};

export default PresentationPlatform;
