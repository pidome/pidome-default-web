/**
 * Helper to provide the correct icon for any hardware type.
 * @returns {HardwareIconHelper}
 */

export const HardwareIconHelper = {
    icon
}

/**
 * Returns the icon for an hardware interface.
 * @param givenIcon The hardware interface
 * @returns {string[]}
 */
function icon (givenIcon) {
    switch (givenIcon) {
        case 'USB':
            return ['fab', 'usb'];
        case 'SERIAL':
        case 'LPT':
            return ['fab', 'plug'];
        case 'NETWORK':
            return ['fab', 'ethernet'];
        case 'SERVER':
            return ['fab', 'server'];
        default:
            return ['fab', 'question'];
    }
}

