
class ToasterImpl {

    /**
     * Sets the vue instance.
     * @param toasterInstance The toast instance.
     */
    setToastInstance(toasterInstance){
        this.toaster = toasterInstance;
    }

    /**
     * Toast a message
     * @param severity The severity of the message.
     * @param text The text to show in a toast.
     */
    toast(headers, text){
        if(text !== undefined && text !== null) {
            switch (headers.severity) {
                case 'MINOR':
                    this.toaster.warning(text);
                    break;
                case 'MAJOR':
                    this.toaster.error(text);
                    break;
                case 'CRITICAL':
                    this.toaster.show(text, {'color': 'purple', 'icon': 'error'});
                    break;
                default:
                    this.toaster.info(text);
                    break;
            }
        }
    }

}
let Toaster = new ToasterImpl();
export default Toaster;
