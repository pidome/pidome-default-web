/*
 * Router.
 */
import Vue from 'vue';
import Router from 'vue-router';
import { store } from '@/store/index';

/**
 * Router options.
 * @type Array
 */
const routerOptions = [
    {path: '/dashboard', component: '/dashboard/Dashboard.vue', meta: { title: 'Dashboard' }},
    {path: '/items', component: '/items/Items.vue', meta: { title: 'Items' }},
    {path: '/login', component: '/login/Login.vue', meta: { title: 'Login' }},
    {path: '/users/me', component: '/users/Me.vue', meta: { title: 'You' }},
    {path: '/users', component: '/users/Users.vue', meta: { title: 'Users' }},
    {path: '/users/:userId', component: '/users/Users.vue', meta: { title: 'User' }},
    {path: '/settings/premises', component: '/settings/premises/premises.vue', meta: { title: 'Premises' }},
    {path: '/settings/hardware', component: '/settings/hardware/hardware.vue', meta: { title: 'Hardware' }},
    {path: '/settings/items/settings', component: '/settings/items/ItemSettings.vue', meta: { title: 'Item settings' }},
    {path: '/settings/modules/active', component: '/settings/modules/Modules.vue', meta: { title: 'Modules' }},
    {path: '/settings/modules/add', component: '/settings/modules/ModuleAdd.vue', meta: { title: 'Add module' }},
    {path: '/settings/items/discovery', component: '/settings/items/ItemsDiscovery.vue', meta: { title: 'Discovery' },
        children: [
            { path: ':discoveredItem', meta: { title: 'Discovery' } },
            { path: ':discoveredItem/:discoveredItemDefinition', meta: { title: 'Discovery' }, name: 'discoveryPredefinedItemDefinition' }
        ]},
    {path: '/settings/items/builders', component: '/settings/items/ItemsBuilder.vue', meta: { title: 'Builders' },
        children: [
            { path: ':builder', meta: { title: 'Builder' } },
            { path: ':builder/discovered/:discoveredId', meta: { title: 'Builder' } }
            ]
    }
];

/**
 * Set route sources.
 * @param {Array} router path options.
 * @type Object router options with components mapped.
 */
const routes = routerOptions.map(route => {
    return {
        ...route,
        component: () => import('@/views' + route.component)
    };
});

Vue.use(Router);

const router = new Router({
    routes
});

router.beforeEach((to, from, next) => {
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);
    if(nearestWithTitle){
        document.title = nearestWithTitle.meta.title + ' - PiDome';
    } else {
        document.title = 'PiDome';
    }
    next();
});

/**
 * Route listener to handle after every route change.
 */
router.afterEach((to) => {
    try {
        store.dispatch('setPageRefreshable', to.matched[0].components.default.data().contentRefreshable);
    } catch(err){
        store.dispatch('setPageRefreshable', false);
    }
});

export default router;
