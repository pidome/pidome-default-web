import HardwareService from './services/HardwareService';
import UserService from './services/UserService';
import ModuleService from "./services/ModuleService";
import ItemService from "./services/ItemService";

/**
 * Methods export.
 * @type type
 */
export const Services = {
    init
};

/**
 * Inititializes classes that needs this.
 */
function init(){
    HardwareService.init();
    UserService.init();
    ModuleService.init();
    ItemService.init();
}
