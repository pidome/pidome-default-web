import Vue from 'vue';
import { store } from '@/store/index';
import { HttpService } from '@/core/services/HttpService';
import VueLocalStorage from 'vue-localstorage';
import router from '@/core/router';
import axios from 'axios';
import { WebSocket } from '@/core/services/WebSocket';
import Toaster from '@/core/helpers/Toaster';

Vue.use(VueLocalStorage);

/**
 * Methods export.
 * @type type
 */
class UserServiceImpl {

    /**
     * Initializes the class.
     */
    init(){
        WebSocket.addListener("org.pidome.server.services.authentication", this.onEvent);
    }

    /**
     * Called when a message is received from the websocket.
     * @param message The message received.
     */
    onEvent(message) {
        if(message.headers.severity === "MAJOR" && message.headers.type === "DELETE"){
            Toaster.toast(message.headers, {}.hasOwnProperty.call(message,"message") ? message.message : 'Change in user session');
            this.logout();
        }
    }

    /**
     * Returns a default empty user skeleton.
     * @returns {getDefaultEmptyUser.UserServiceAnonym$0}
     */
    getDefaultEmptyUser() {
        return {
            'person': {}
        };
    }

    /**
     * Validates a token.
     * @param {String} routePath The route to take when token appears to be valid.
     * @returns {void}
     */
    validateToken(routePath) {
        let token = Vue.localStorage.get('user.token', '', String);
        if (token === '') {
            this.logout();
        } else {
            store.dispatch('login', {
                token: token
            });
            this.getSelfUser().then(userResult => {
                store.dispatch('setUser', {
                    user: userResult.data
                });
                store.dispatch('loginCompleted');
                router.push(routePath);
                WebSocket.connect();
            }).catch(() => {
                this.logout();
                WebSocket.disconnect();
            });
        }
    }

    /**
     * Revokes a token.
     * @param tokenReferenceId reference id to the token.
     * @returns {Promise}
     */
    revokeToken(tokenReferenceId) {
        return HttpService.restDeleteRequest('/auth/tokens/' + tokenReferenceId);
    }

    /**
     * PAssword strength checker.
     * @param password The password to check.
     * @returns {Promise<unknown>}
     */
    checkPasswordStrength(password){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/auth/service/password/strength/' + encodeURIComponent(password)).then(result => {
                resolve(result.data);
            }).catch(error => {
                reject(error);
            });
        });
    }

    /**
     * Enpoint which tries to identify this local resource.
     * @returns {Promise<unknown>}
     */
    identifyLocalResource(){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/auth//service/ua').then(result => {
                resolve(result.data);
            }).catch(error => {
                reject(error);
            });
        });
    }

    /**
     * Updates the password for the given user.
     * @param {String} oldPass the current password.
     * @param {String} pass The new password.
     * @param {String} passAgain The new password again.
     * @returns {Promise}
     */
    updatePassword(oldPass, pass, passAgain) {
        return HttpService.restPatchRequest('/users/me/password', {
            oldPassword: oldPass,
            newPassword: pass,
            retypeNewPassword: passAgain
        });
    }

    /**
     * Perform user login.
     *
     * The login routine does not make use of the regular HttpService.api method as this
     * method also checks if the remote service is the correct one and then sets the api
     * resources so upfollowing requests will be using the correct one.
     *
     * @param {String} host The password.
     * @param {Integer} port The password.
     * @param {String} path The password.
     * @param {String} username The username.
     * @param {String} password The password.
     * @returns {Promise}
     */
    login(host, port, path, username, password) {
        HttpService.clearInstance();
        return new Promise((resolve, reject) => {
            axios.post(`https://${host}:${port}/${path}/auth/service/login`, {
                username: username,
                password: password
            }).then(response => {
                HttpService.setHttpResources(host, port, '/' + path);
                resolve(response.data);
            }).catch(error => {
                if (error.response) {
                    if (error.response.status === 401) {
                        HttpService.setHttpResources(host, port, '/' + path);
                    }
                }
                reject(error);
            });
        });
    }

    /**
     * Returns the user self object.
     * @returns {Promise}
     */
    getSelfUser() {
        return HttpService.restGetRequest('/users/me');
    }

    /**
     * Returns the self user.
     * @returns {Promise}
     */
    getMe() {
        return this.getSelfUser().then(userResult => {
            store.dispatch('setUser', {
                user: userResult.data
            });
        });
    }

    /**
     * Updates the user.
     * @param {User} user
     * @returns {Promise}
     */
    addUser(user) {
        return new Promise((resolve, reject) => {
            HttpService.restPostRequest('users', user).then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
        });
    }

    /**
     * Perform user logout.
     * @returns {void}
     */
    logout() {
        HttpService.restGetRequest('/auth/service/logout');
        store.dispatch('logout');
        Vue.localStorage.set('user.token', '');
        router.push('/login');
        HttpService.clearInstance();
        //WebSocket.disconnect();
    }

    /**
     * Returns if someone is logged in or not.
     * @returns {Boolean} observable for logged in updates.
     */
    isLoggedIn() {
        return store.state.User.isLoggedIn;
    }

    /**
     * Returns a list of users.
     * @returns {Promise} user list object on success.
     */
    getUsers() {
        return new Promise((resolve, reject) => {
            HttpService.restGetRequest('/users').then(result => {
                resolve({
                    'amount': 0,
                    'index': 0,
                    'size': 0,
                    'users': result.data
                });
            }).catch(error => {
                reject(error);
            });
        });
    }

    /**
     * Returns an user with the given id.
     * @param {Integer} id The id of the user.
     * @returns {Promise} User structure or error.
     */
    getUser(id) {
        return new Promise((resolve, reject) => {
            HttpService.restGetRequest('/users/user/' + id).then(result => {
                resolve(result.data);
            }).catch(error => {
                reject(error);
            });
        });
    }

    /**
     * Returns a list of active tokens for the current logged in user.
     * @returns {Promise} Tokens list or error.
     */
    getActiveTokens() {
        return new Promise((resolve, reject) => {
            HttpService.restGetRequest('/auth/tokens').then(result => {
                resolve(result.data);
            }).catch(error => {
                reject(error);
            });
        });
    }

    /**
     * Returns the url to generate a self QR code.
     * @returns {Promise} The path to the QR code generator.
     */
    generateQr() {
        return new Promise((resolve, reject) => {
            HttpService.restGetBinaryData('/auth/qr', 'image/png').then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
        });
    }
}

const UserService = new UserServiceImpl();
export default UserService;
