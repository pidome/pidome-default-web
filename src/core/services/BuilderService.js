import { HttpService } from '@/core/services/HttpService';

class BuilderServiceImpl {

    /**
     * Returns a builder configuration for a discovered device.
     * @param discoveryId The id of the discovered device.
     * @returns {Promise<unknown>} Promise with a builder configuration.
     */
    getBuilderDataFromDiscovered(discoveryId){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/builder/discovery/devices/' + discoveryId)
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Creates a device definition.
     *
     * @param definition
     * @returns {Promise<unknown>}
     */
    createDeviceDefinition(definition){
        return new Promise((resolve, reject) => {
            return HttpService.restPostRequest('/builder/create/', definition)
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

}

const BuilderService = new BuilderServiceImpl();
export default BuilderService;
