import { HttpService } from '@/core/services/HttpService';
import Toaster from '@/core/helpers/Toaster';
import {WebSocket} from "./WebSocket";
import EventBus from '@/core/EventBus';

class ItemServiceImpl {


    /**
     * Called when a message is received from the websocket.
     * @param message The message received.
     */
    onEvent(message) {
        if ({}.hasOwnProperty.call(message, "message")) {
            if({}.hasOwnProperty.call(message, "body")){
                if(message.body.type === 'DISCOVERED') {
                    Toaster.toast(message.headers,
                        message.body.items.length + " item" +
                        ((message.body.items.length > 1) ? "s " : " ") +
                        ((message.body.type === "DISCOVERED") ? "discovered" : "removed from discovery"));
                    EventBus.$emit('content', 'refresh.discovery');
                } else if (message.headers.type === 'ADD' || message.headers.type === 'REMOVED'){
                    EventBus.$emit('content', 'refresh.items');
                } else if (message.headers.type === 'NOTIFY'){
                    if({}.hasOwnProperty.call(message.body, "dataNotification")){
                        EventBus.$emit('item.notify', message.body);
                    }
                }
            }
        }
    }

    /**
     * Initializes the class.
     */
    init(){
        WebSocket.addListener("org.pidome.server.services.items", this.onEvent);
    }

    /**
     * Returns the active items available on the server.
     * @returns {Promise<any>}
     */
    getItems(){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/items')
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Returns all known items.
     * @returns {Promise<any>}
     */
    getAllItems(){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/items/all')
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Returns the definition of an item.
     * @param itemId The id of the item to get the definition for.
     * @returns {Promise<unknown>}
     */
    getItemDefinition(itemId){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/items/item/'+itemId+'/definition')
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Returns a list of item definitions for the given item type.
     * @param itemType the item type.
     * @returns {Promise<unknown>} Promise with definitions.
     */
    getItemDefinitions(itemType){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/items/definitions/' + itemType)
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Returns a list of discovered items.
     * @returns {Promise<any>}
     */
    getDiscoveredItems(){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/items/discovered')
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Returns a single discovered item with the given itemId.
     * @param itemId Id of the item to get.
     * @returns {Promise<any>}
     */
    getDiscoverdItem(itemId){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/items/discovered/' + itemId)
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Adds an item to the server.
     * @param itemDefinitionId The definition id of the item.
     * @param itemInfo The information of the item.
     * @returns {Promise<unknown>}
     */
    addItem(itemDefinitionId, itemInfo){
        return new Promise((resolve, reject) => {
            return HttpService.restPostRequest('/items/item/' + itemDefinitionId, itemInfo)
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Update an item
     * @param itemInfo The item info to update.
     * @returns {Promise<unknown>}
     */
    updateItem(itemInfo){
        return new Promise((resolve, reject) => {
            return HttpService.restPutRequest('/items/item/' + itemInfo.id, itemInfo)
                .then(() => {
                    resolve(true);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Update an item
     * @param itemInfo The item info to update.
     * @returns {Promise<unknown>}
     */
    deleteItem(itemInfo){
        return new Promise((resolve, reject) => {
            return HttpService.restDeleteRequest('/items/item/' + itemInfo.id)
                .then(() => {
                    resolve(true);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Adds an item to the server.
     * @param discoveredItemId The discoveredItem
     * @param itemInfo The item info object.
     * @returns {Promise<any>}
     */
    addFromDiscoveredItem(discoveredItemId, itemDefinitionId,  itemInfo){
        return new Promise((resolve, reject) => {
            return HttpService.restPostRequest('/items/discovered/' + discoveredItemId + '/' + itemDefinitionId, itemInfo)
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

}
const ItemService = new ItemServiceImpl();
export default ItemService;
