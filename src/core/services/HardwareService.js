import { HttpService } from '@/core/services/HttpService';
import { WebSocket } from "./WebSocket";
import Toaster from '@/core/helpers/Toaster';
import EventBus from '@/core/EventBus';

class HardwareServiceImpl {

    /**
     * Called when a message is received from the websocket.
     * @param message The message received.
     */
    onEvent(message) {
        Toaster.toast(message.headers, {}.hasOwnProperty.call(message,"message") ? message.message : 'Change in attached hardware');
        EventBus.$emit('content', 'refresh.hardware');
    }

    /**
     * Initializes the class.
     */
    init(){
        WebSocket.addListener("org.pidome.server.services.hardware", this.onEvent);
    }

    /**
     * Returns the list of current active and inactive hardware registered on the server.
     * @returns {Promise}
     */
    getCurrentHardware() {
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/hardware')
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Returns a freeform statistics presentation object.
     * @param peripheralId The id of the peripheral to gather the stats from.
     * @returns {Promise}
     */
    getHardwareStats(peripheralId){
        return HttpService.restGetRequest('/hardware/peripheral/' + peripheralId + '/stats');
    }

    /**
     * Returns the hardware present on the given interface, active or not.
     * @param hardwareInterface The interface to get the hardware from.
     * @returns {Promise}
     */
    getCurrentHardwareOnInterface (hardwareInterface) {
        return HttpService.restGetRequest('/hardware/interface/' + hardwareInterface);
    }

    /**
     * Returns the hardware present on the given interface, active or not.
     * @param hardwareInterface The interface to get the hardware from.
     * @param hardwareKey The hardware key.
     * @returns {Promise}
     */
    getCurrentHardwareDevice (hardwareInterface, hardwareKey) {
        return HttpService.restGetRequest('/hardware/interface/' + hardwareInterface + '/' + encodeURIComponent(hardwareKey));
    }

    /**
     * Returns a list of driver candidates for the given interface with the given hardware key.
     * @param hardwareInterface The interface the device is available on.
     * @param hardwareKey The key for the hardware device.
     * @returns {Promise}
     */
    getCurrentHardwareDeviceDriverCandidates(volatilePeripheralId) {
        return HttpService.restGetRequest('/hardware/peripheral/' + volatilePeripheralId + '/drivers');
    }

    /**
     * Returns driver settings for the given interface, key, package and driver path.
     * @param volatilePeripheralId The key of the peripheral.
     * @param driverDefinitionId The key of the driver definition.
     * @returns {Promise}
     */
    getCurrentHardwareDeviceDriverSettings (volatilePeripheralId, driverDefinitionId) {
        return HttpService.restGetRequest('/hardware/peripheral/' + volatilePeripheralId + '/settings/'+driverDefinitionId);
    }

    /**
     * Returns a list of peripherals filtered on the transport type given.
     * @param transportFilter The transport type to filter on.
     * @returns {Promise}
     */
    getPeripheralTransportFilteredList(transportTarget, filterTarget){
        let filterParameters = '?targets=';
        if(filterTarget !== undefined && filterTarget !== null && filterTarget.length > 0){
            filterParameters += encodeURIComponent(
                JSON.stringify(filterTarget)
                    .replace('[','')
                    .replace(']','')
                    .replace(/"/g,'')
            );
        }
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/hardware/type/' + transportTarget + filterParameters)
                .then(result => {
                resolve(result.data);
            }).catch(error => {
                reject(error);
            });
        });
    }

    /**
     * Starts the hardware on the given parameters.
     * @param volatilePeripheralId Id of the peripheral.
     * @param driverDefinitionId The id of the driver definition.
     * @param configuration The configuration to apply to the driver.
     * @returns {Promise}
     */
    startHardware(volatilePeripheralId, driverDefinitionId, configuration) {
        return new Promise((resolve, reject) => {
            return HttpService.restPutRequest('/hardware/peripheral/' + volatilePeripheralId + '/start/' + driverDefinitionId + '',
                configuration)
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Stops the hardware on the given parameters.
     * @param volatilePeripheralId Id of the peripheral.
     * @returns {Promise}
     */
    stopHardware(volatilePeripheralId) {
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/hardware/peripheral/' + volatilePeripheralId + '/stop')
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }
}
const HardwareService = new HardwareServiceImpl();
export default HardwareService;
