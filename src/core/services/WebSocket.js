import Vue from 'vue';
import VueNativeSock from 'vue-native-websocket'
import { store } from '@/store/index';
import { HttpService } from '@/core/services/HttpService';

/**
 * Use the socket.
 */
Vue.use(VueNativeSock, getWebsocketPath(), {
    connectManually: true,
});

/**
 * Assign var.
 * @type {Vue | CombinedVueInstance<Vue, object, object, object, Record<never, any>>}
 */
const socket = new Vue();

/**
 * Exports methods
 */
export const WebSocket = {
    connect,
    disconnect,
    addListener,
    removeListener
};

/**
 * Collection of domain listeners.
 * @type {Map<String, Service>}
 */
let listeners = new Map();

/**
 * Adds a listener to the listeners list.
 * @param domain The string of the domain to listen to.
 * @param eventMethod The method to execute from a service.
 */
function addListener(domain, eventMethod){
    if(!listeners.has(domain)) {
        listeners.set(domain, eventMethod);
    }
}

/**
 * Removes the listener on the given domain.
 * @param domain The domain to remove the listener for.
 */
function removeListener(domain){
    listeners.delete(domain);
}

/**
 * Connects to the websocket endpoint.
 */
function connect(){
    socket.$connect(getWebsocketPath(), { format: 'json' });
    socket.$options.sockets.onmessage = (messageEvent) => {
        onEvent(messageEvent.data);
    }
}

function onEvent(eventData){
    let messageObject = JSON.parse(eventData);
    if({}.hasOwnProperty.call(messageObject, 'headers') && {}.hasOwnProperty.call(messageObject.headers, 'domain')){
        listeners.forEach( (eventMethod, domain) => {
            if(domain === messageObject.headers.domain){
                eventMethod(messageObject);
            }
        });
    }
}

/**
 * Disconnects from the remote socket.
 */
function disconnect(){
    delete socket.$options.sockets.onmessage;
    socket.$disconnect();
}

/**
 * Returns the path to the socket.
 * @returns {string}
 */
function getWebsocketPath(){
    return 'wss://' + HttpService.getHttpServer() + ':' + HttpService.getHttpPort() + '/events?' + store.state.User.token.token;
}
