import { HttpService } from '@/core/services/HttpService';

/**
 * The premises service exposed methods.
 * @type type
 */
export const PremisesService = {
    getPremisesList,
    getPremises,
    getPropertyLevel,
    getIconCollection
};

/**
 * Returns the premises.
 * @returns {Promise}
 */
function getPremisesList() {
    return HttpService.restGetRequest('/premises');
}

/**
 * Returns a premises promise.
 * @param {type} id The id of the premises to fetch.
 * @returns {Promise} Premises promise return
 */
function getPremises(id) {
    return HttpService.restGetRequest('/premises/' + id);
}

/**
 * Returns a property level by it's id.
 * @param {type} id The id of the property level.
 * @returns {Promise} Promise for the property level.
 */
function getPropertyLevel(id) {
    return HttpService.restGetRequest('/premises/property/level/' + id);
}

/**
 * Returns the icons used for premises locations.
 * @returns {Array}
 */
function getIconCollection() {
    return [
        {'type': 'fa', 'icon': 'person-booth'},
        {'type': 'fa', 'icon': 'restroom'},
        {'type': 'fa', 'icon': 'building'},
        {'type': 'fa', 'icon': 'swimming-pool'},
        {'type': 'fa', 'icon': 'bed'},
        {'type': 'fa', 'icon': 'home'},
        {'type': 'fa', 'icon': 'road'},
        {'type': 'fa', 'icon': 'city'},
        {'type': 'fa', 'icon': 'couch'},
        {'type': 'fa', 'icon': 'hot-tub'},
        {'type': 'fa', 'icon': 'map'},
        {'type': 'fa', 'icon': 'shuttle-van'},
        {'type': 'fa', 'icon': 'store-alt'},
        {'type': 'fa', 'icon': 'school'},
        {'type': 'fa', 'icon': 'toilet'},
        {'type': 'fa', 'icon': 'toilet-paper'},
        {'type': 'fa', 'icon': 'truck-loading'},
        {'type': 'fa', 'icon': 'truck-pickup'},
        {'type': 'fa', 'icon': 'warehouse'},
        {'type': 'fa', 'icon': 'gamepad'},
        {'type': 'fa', 'icon': 'mug-hot'},
        {'type': 'fa', 'icon': 'utensils'},
        {'type': 'fa', 'icon': 'door-open'},
        {'type': 'fa', 'icon': 'car'},
        {'type': 'fa', 'icon': 'baby-carriage'},
        {'type': 'fa', 'icon': 'coffee'},
        {'type': 'fa', 'icon': 'cocktail'},
        {'type': 'fa', 'icon': 'child'},
        {'type': 'fa', 'icon': 'baby'},
        {'type': 'fa', 'icon': 'male'},
        {'type': 'fa', 'icon': 'female'},
        {'type': 'fa', 'icon': 'sticker-mule'},
        {'type': 'fa', 'icon': 'user-friends'},
        {'type': 'fa', 'icon': 'minus'},
        {'type': 'mat', 'icon': 'all_inbox'},
        {'type': 'mat', 'icon': 'music_video'},
        {'type': 'mat', 'icon': 'movie'},
        {'type': 'mat', 'icon': 'album'},
        {'type': 'mat', 'icon': 'mic'},
        {'type': 'mat', 'icon': 'storage'},
        {'type': 'mat', 'icon': 'computer'},
        {'type': 'mat', 'icon': 'videogame_asset'}
    ];
}