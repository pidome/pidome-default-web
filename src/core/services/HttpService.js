import Vue from 'vue';
import { store } from '@/store/index';
import VueLocalStorage from 'vue-localstorage';
import axios from 'axios';
import EventBus from '@/core/EventBus';

Vue.use(VueLocalStorage);

/**
 * The axios instance.
 * @type Axios
 */
var httpAxiosInstance,
    /**
     * The promise used as interceptor for axios.
     * @type Promise
     */
    errorPopup;


/**
 * Methods export.
 * @type HttpService
 */
export const HttpService = {
    restGetRequest,
    restPostRequest,
    restPutRequest,
    restPatchRequest,
    restDeleteRequest,
    setHttpResources,
    setHttpSocketResources,
    getHttpServer,
    getHttpPort,
    getHttpEndpoint,
    clearInstance,
    registerErrorPopup,
    getHttpRestPath,
    restGetBinaryData
};

/**
 * A get request.
 * @param path {String} The path to the get request.
 * @param urlParameters {Object} an object with key value pairs to add to the request, can be ommitted if not needed.
 * @returns {Promise} A promise containing the result or request error.
 */
function restGetRequest(path, urlParameters) {
    return axiosInstance()
            .get(path + setUrlParameters(urlParameters));
}

/**
 * A post request.
 * @param {String} path The path to post to, required.
 * @param {Object} postObject The object to post, required.
 * @param {Object} urlParameters The url parameters, may be omitted.
 * @returns {Promise} A promise containing the result or request error.
 */
function restPostRequest(path, postObject, urlParameters) {
    return axiosInstance()
            .post(path + setUrlParameters(urlParameters), postObject);
}

/**
 * A put request.
 * @param {String} path The path to post to, required.
 * @param {Object} postObject The object to post, required.
 * @param {Object} urlParameters The url parameters, may be omitted.
 * @returns {Promise} A promise containing the result or request error.
 */
function restPutRequest(path, postObject, urlParameters) {
    return axiosInstance()
        .put(path + setUrlParameters(urlParameters), postObject);
}

/**
 * A patch request.
 * @param {String} path The path to post to, required.
 * @param {Object} patchObject The object to post, required.
 * @param {Object} urlParameters The url parameters, may be omitted.
 * @returns {Promise} A promise containing the result or request error.
 */
function restPatchRequest(path, patchObject, urlParameters) {
    return axiosInstance()
            .patch(path + setUrlParameters(urlParameters), patchObject);
}

/**
 * A delete request.
 * @param {String} path The path to post to, required.
 * @param {Object} urlParameters The url parameters, may be omitted.
 * @returns {Promise} A promise containing the result or request error.
 */
function restDeleteRequest(path, urlParameters) {
    return axiosInstance()
            .delete(path + setUrlParameters(urlParameters));
}

/**
 * Returns binary data from a get request.
 * @param {String} path The path to post to, required.
 * @param {String} responseType The type receiving.
 * @param {Object} urlParameters The url parameters, may be omitted.
 * @returns {Binary}
 */
function restGetBinaryData(path, responseType, urlParameters) {
    return axiosInstance()
            .get(path + setUrlParameters(urlParameters), {
                responseType: 'arraybuffer'
            });
}

/**
 * Creates an axios base configuration to make requests with.
 * @returns {Axios} a base configuration.
 */
function axiosInstance() {
    if (httpAxiosInstance === undefined) {
        httpAxiosInstance = axios.create({
            baseURL: getHttpRestPath(),
            headers: {'Authorization': 'Bearer ' + store.state.User.token.token}
        });
        httpAxiosInstance.interceptors.response.use(function (response) {
            EventBus.$emit("content.wait", false);
            return response;
        }, function (err) {
            //console.log(JSON.parse(JSON.stringify(err)));
            if (err === undefined || !{}.hasOwnProperty.call(err, 'response') || err.response === undefined || !{}.hasOwnProperty.call(err.response,"status")) {
                errorPopup.showRestError("Unexpected error occured", err);
            } else {
                if (err.response.status === 401 && err.config && !err.config.__isRetryRequest) {
                    if (err.config.url.indexOf('auth/login') === -1 && err.config.url.indexOf('auth/logout') === -1) {
                        errorPopup.showError("Authorization error", "Not logged in or not authorized to do the request.");
                    }
                } else if (err.response.status === 404) {
                    errorPopup.showRestError("Not found", err);
                } else if (err.response.status === 403) {
                    errorPopup.showRestError("Not allowed", err, "Not allowed to access the requested resource");
                } else if (err.response.status === 500) {
                    errorPopup.showRestError("A Server error occurred", err);
                } else {
                    errorPopup.showRestError("An error occurred", err);
                }
            }
            EventBus.$emit("content.wait", false);
            return Promise.reject(err);
        });
    }
    return httpAxiosInstance;
}

/**
 * Returns url parameters.
 * @param parameters {Object} an object with key value pairs to add to the request, supports undefined.
 * @returns {String} empty string or with parameters if not undefined.
 */
function setUrlParameters(parameters) {
    if (parameters !== undefined) {
        let params = "?";
        Object.keys(parameters).forEach(function (key) {
            params += (key + '=' + parameters[key]) + '&';
        });
        return params.substring(0, params.length-1);
    }
    return "";
}

/**
 * Sets the http REST endpoint values.
 * @param {type} host The host where the endpoint is.
 * @param {type} port The port of the server.
 * @param {type} endpoint The endpoint url.
 * @returns {void}
 */
function setHttpResources(host, port, endpoint) {
    Vue.localStorage.set('http.server', host);
    Vue.localStorage.set('http.port', port);
    Vue.localStorage.set('http.endpoint', endpoint);
}

/**
 * Sets the websocket values.
 * @param {Integer} port The websocket port.
 * @param {String} endpoint The websocket endpoint.
 * @returns {void}
 */
function setHttpSocketResources(port, endpoint) {
    Vue.localStorage.set('http.socket.port', port);
    Vue.localStorage.set('http.socket.endpoint', endpoint);
}

/**
 * The server host.
 *
 * Can be ip or hostname.
 *
 * @returns {String}
 */
function getHttpServer() {
    return Vue.localStorage.get('http.server', window.location.hostname, String);
}
/**
 * The server port
 * @returns {Integer}
 */
function getHttpPort() {
    return Vue.localStorage.get('http.port', 8080, Number);
}
/**
 * Returns the endpoint for the REST calls.
 * @returns {String}
 */
function getHttpEndpoint() {
    return Vue.localStorage.get('http.endpoint', '/api', String);
}

/**
 * Clears the axios instance to be able to re register.
 * @returns {void}
 */
function clearInstance() {
    httpAxiosInstance = undefined;
}

/**
 * Registers a promise to be used when a new axios instance is to be created.
 * @param {Object} popup A vue popup template instance.
 * @returns {void}
 */
function registerErrorPopup(popup) {
    errorPopup = popup;
}

/**
 * Returns the http path.
 * @returns {String}
 */
function getHttpRestPath() {
    return 'https://' + getHttpServer() + ':' + getHttpPort() + getHttpEndpoint();
}

/**
 * Returns the port on witch the websocket port runs.
 * @returns {Integer}

 function getHttpSocketPort() {
 return Vue.localStorage.get('http.socket.port', 8081, Number);
 }
 /**
 * Returns the base endpoint for the websocket.
 * @returns {String}

 function getHttpSocketEndpoint() {
 return Vue.localStorage.get('http.socket.endpoint', '/socket', String);
 }
 */
