import { HttpService } from '@/core/services/HttpService';

/**
 * Helper to interaction for device items.
 * @returns {HardwareIconHelper}
 */
export const ItemsDeviceHelper = {
    updateDeviceData,
    deviceDefaultEmptyConfiguration,
    discoveredDeviceDefaultEmptyConfiguration,
    sendDeviceCommand
};

function sendDeviceCommand(deviceId, groupId, controlId, value){
    return new Promise((resolve, reject) => {
        let deviceCommand = {
            type: 'COMMAND',
            itemId: deviceId,
            itemCommand: {
                type: 'DEVICE',
                action: {
                    group: groupId,
                    control: controlId,
                    value: value
                }
            }
        };
        HttpService.restPatchRequest('items/item/'+deviceId, deviceCommand)
            .then(result => {
                resolve(result.data);
            }).catch(error => {
                reject(error);
            });
    });

}

/**
 * Updates a device's data.
 * @param devices Devices list
 * @param updateData Update data from the server.
 * @returns {void}
 */
function updateDeviceData (devices, updateData) {
    if({}.hasOwnProperty.call(updateData, 'dataNotification') && updateData.dataNotification.itemType === 'DEVICE') {
        for(let i = 0; i < devices.length; i++) {
            if(updateData.dataNotification.deviceId === devices[i].id) {
                let device = devices[i];
                let itemUpdateData = updateData.dataNotification;
                for (let g = 0; g < device.groups.length; g++) {
                    let group = device.groups[g];
                    for (let gd = 0; gd < itemUpdateData.groupData.length; gd++) {
                        if (itemUpdateData.groupData[gd].id === group.id) {
                            let groupData = itemUpdateData.groupData[gd];
                            for (let c = 0; c < group.controls.length; c++) {
                                let control = group.controls[c];
                                for (let cd = 0; cd < groupData.controlData.length; cd++) {
                                    if (groupData.controlData[cd].id == control.id) {
                                        control.value = groupData.controlData[cd].value;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

/**
 * Returns a default empty device configuration.
 * @returns {{privateFields: *, address: {address: string, dataType: string, description: string, label: string}, name: string, options: *, description: string}}
 */
function deviceDefaultEmptyConfiguration(){
    let construct = {
        name: '',
        description: '',
        address: '',
        options: Object
    };
    return construct;
}

/**
 * Returns an empty configuration for a discovered device.
 * @returns {{address: ObjectConstructor, name: string, options: ObjectConstructor, description: string}}
 */
function discoveredDeviceDefaultEmptyConfiguration(){
    let construct = this.deviceDefaultEmptyConfiguration();
    construct.discoveredId = '';
    return construct;
}
