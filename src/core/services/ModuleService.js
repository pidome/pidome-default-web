import { HttpService } from '@/core/services/HttpService';
import Toaster from '@/core/helpers/Toaster';
import {WebSocket} from "./WebSocket";
import EventBus from '@/core/EventBus';

class ModuleServiceImpl {


    /**
     * Called when a message is received from the websocket.
     * @param message The message received.
     */
    onEvent(message) {
        if({}.hasOwnProperty.call(message,"message")) {
            Toaster.toast(message.headers, message.message);
            EventBus.$emit('content', 'refresh.modules');
        }
    }

    /**
     * Initializes the class.
     */
    init(){
        WebSocket.addListener("org.pidome.server.services.modules", this.onEvent);
    }

    /**
     * Returns a default module configuration object.
     * @returns {{peripheral: ObjectConstructor, moduleDefinition: ObjectConstructor, driverConfiguration: ObjectConstructor, driverDefinition: ObjectConstructor, moduleConfiguration: ObjectConstructor}}
     */
    moduleDefaultEmptyConfiguration(){
        return {
                moduleDefinition: Object,
                moduleConfiguration: Object,
                peripheral: Object,
                driverDefinition: Object,
                driverConfiguration: Object
        }
    }

    /**
     * Translates a module type key to a representative name.
     * @param typeKey The type key.
     * @returns {Object} The representative string.
     */
    moduleTypeTranslator(typeKey){
        if (typeKey === undefined){
            return {description: '', icon: 'questionmark'};
        }
        switch(typeKey.toUpperCase()){
            case 'DEVICES':
                return {description: 'Devices', icon: 'microchip'};
        }
    }

    /**
     * Returns a promise with modules found with the given parameters.
     * @param capabilitiesList List of requested capabilities.
     * @returns {Promise<Module>}
     */
    getModulesWithCapabilities(capabilitiesList){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/modules/capabilities', {'list': capabilitiesList.join(',')})
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Returns the full modules store.
     * @returns {Promise<Modules>}
     */
    getAvailableModules(){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/modules')
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Returns information about a module.
     * @param containerId The id of the container.
     * @returns {Promise<Presentation>}
     */
    getModuleInfo(containerId){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/modules/module/' + containerId + '/info')
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Returns discovery options.
     * @returns {Promise<any>}
     */
    getDiscoveryOptions(){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/modules/discovery/options')
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * starts the discovery for items on the given module.
     * @param containerId The active container id.
     * @param discoveryOptions The discovery options.
     * @returns {Promise<any>}
     */
    startItemDiscovery(containerId, discoveryOptions){
        return new Promise((resolve, reject) => {
            return HttpService.restPutRequest('/modules/module/' + containerId + '/discovery/start', discoveryOptions)
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Starts a module with the given configuration.
     * @param moduleConfiguration The configuration to post.
     * @returns {Promise<any>}
     */
    startModule(moduleConfiguration){
        return new Promise((resolve, reject) => {
            return HttpService.restPostRequest('/modules/module/start', moduleConfiguration)
                .then(result => {
                    resolve(result.data);
                    Toaster.toast({'severity':'CRITICAL'}, 'Module started');
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Returns a list of currently active modules.
     * @returns {Promise<any>}
     */
    getActiveModules(){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/modules/active')
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    /**
     * Stops a module from running.
     * @param moduleId The id of the running module.
     * @returns {Promise} result.
     */
    stopModule(moduleId){
        return new Promise((resolve, reject) => {
            return HttpService.restGetRequest('/modules/module/'+moduleId+'/stop')
                .then(result => {
                    resolve(result.data);
                }).catch(error => {
                    reject(error);
                });
        });
    }

}
const ModuleService = new ModuleServiceImpl();
export default ModuleService;
