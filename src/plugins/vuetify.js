import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue'
import Vuetify, { VSnackbar } from 'vuetify/lib'

import VeeValidate from 'vee-validate';
import VueMoment from 'vue-moment';
import VuetifyToast from 'vuetify-toast-snackbar';

Vue.use(
    Vuetify, {
      components: {
        VSnackbar
      }
    });
Vue.use(VueMoment);
Vue.use(VeeValidate);
Vue.use(VuetifyToast);

export default new Vuetify({
    global: {
        ripples: false
    },
    icons: {
        iconfont: 'mdi',
    },
    /*
    theme: {
        themes: {
            light: {
                "primary": "#ff5722",
                "secondary": "#3f51b5",
                "accent": "#ff6e40",
                "error": "#FF5252",
                "info": "#2196F3",
                "success": "#4CAF50",
                "warning": "#FB8C00"
            }
        }
    }
     */
});
