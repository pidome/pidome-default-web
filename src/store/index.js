import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import {UserStoreModule} from '@/store/modules/UserStoreModule';
import {ApplicationStoreModule} from '@/store/modules/ApplicationStoreModule';

export const store = new Vuex.Store({
    strict: process.env.NODE_ENV !== 'production',
    modules: {
        User: UserStoreModule,
        Application: ApplicationStoreModule
    }

});