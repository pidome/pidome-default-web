import Vue from 'vue';
import VueLocalStorage from 'vue-localstorage';
Vue.use(VueLocalStorage);

/**
 * The user store module.
 *
 * The store for the user.
 *
 * @type UserStoreModule
 */
export const UserStoreModule = {
    state: {
        loggedIn: false,
        token: '',
        user: {}
    },
    mutations: {
        login(state, { token }) {
            state.token = token;
        },
        completed(state) {
            Vue.localStorage.set('user.token', state.token.token);
            state.loggedIn = true;
        },
        logout(state) {
            state.loggedIn = false;
            state.token = '';
            state.user = {};
        },
        user(state, { user }) {
            state.user = user;
        }
    },
    actions: {
        setUser( { commit }, user) {
            commit('user', {
                user
            });
        },
        login( { commit }, token) {
            commit('login', {
                token
            });
        },
        loginCompleted( { commit }){
            commit('completed');
        },
        logout( { commit }) {
            commit('logout');
        }
    },
    getters: {
        isLoggedIn: state => state.loggedIn,
        getUser: state => state.user,
        getToken: state => state.token
    }
};
