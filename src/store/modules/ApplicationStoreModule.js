import Vue from 'vue';
import VueLocalStorage from 'vue-localstorage';
Vue.use(VueLocalStorage);

/**
 * Store for application specific settings or items.
 *
 * @type ApplicationStoreModule
 */
export const ApplicationStoreModule = {
    state: {
        darkThemeAutomatic: Vue.localStorage.get('application.dark.automatic', false, Boolean),
        isCurrentlyDark: Vue.localStorage.get('application.dark', false, Boolean),
        pageContentRefreshable: false
    },
    mutations: {
        switchDarkThemeAutomatic(state, { value }) {
            state.darkThemeAutomatic = value.value;
        },
        switchDarkThemeManual(state, { value }) {
            state.isCurrentlyDark = value.value;
        },
        setPageRefreshable(state, {value}) {
            state.pageContentRefreshable = value;
        }
    },
    actions: {
        switchDarkThemeAutomatic( { commit }, value) {
            Vue.localStorage.set('application.dark.automatic', value.value);
            commit('switchDarkThemeAutomatic', {
                value: value
            });
        },
        switchDarkThemeManual( { commit }, value) {
            Vue.localStorage.set('application.dark', value.value);
            commit('switchDarkThemeManual', {
                value: value
            });
        },
        setPageRefreshable( { commit }, value){
            commit('setPageRefreshable', { value: value });
        }
    },
    getters: {
        isDarkThemeAutomatic: state => state.darkThemeAutomatic,
        isCurrentlyDark: state => state.isCurrentlyDark,
        isPageRefreshable: state => state.pageContentRefreshable
    }
};
